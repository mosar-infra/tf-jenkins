# jenkins/root/main.tf

module "networking" {
  source               = "git::https://gitlab.com/mosar-infra/tf-module-networking.git?ref=tags/v1.0.6"
  public_subnet_count  = 0
  private_subnet_count = 3
  vpc_cidr             = local.vpc_cidr
  max_subnets          = 20
  public_cidrs         = [for i in range(2, 255, 2) : cidrsubnet(local.vpc_cidr, 8, i)]
  private_cidrs        = [for i in range(1, 255, 2) : cidrsubnet(local.vpc_cidr, 8, i)]
  cidr_security_groups = local.cidr_security_groups
  sg_security_groups   = local.sg_security_groups
  create_natgw         = true
}

# module "loadbalancing" {
#   source                    = "git::https://gitlab.com/mosar-infra/tf-module-loadbalancing.git?ref=tags/v1.0.0"
#   lb_name                   = "jenkins-lb"
#   public_subnet_ids         = module.networking.public_subnet.*.id
#   public_security_group_ids = module.networking.public_security_groups.*.id
#   vpc_id                    = module.networking.vpc_id
#   environment               = "jenkins"
#   certificate_domains       = local.certificate_domains
#   listener_port             = 443
#   listener_protocol         = "HTTPS"
#   ssl_policy                = "ELBSecurityPolicy-2016-08"
#   lb_target_groups          = local.lb_target_groups
# }

module "computing" {
  source                  = "git::https://gitlab.com/mosar-infra/tf-module-computing.git?ref=tags/v1.0.0"
  public_key_path         = "./mosar_server_ssh.pub"
  ami_name_string         = "amzn2-ami-hvm-2.0*"
  ami_owners              = ["amazon"]
  key_name                = "mosar_server_ssh"
  nodes                   = local.nodes
  lb_tg_attachments       = []
  lb_target_groups        = []
  private_subnet_cidr_map = module.networking.private_subnet_cidr_map
}

# module "secrets" {
#   source      = "git::https://gitlab.com/mosar-infra/tf-module-secrets.git?ref=tags/v1.0.1"
#   secrets     = var.secrets
# }

module "iam" {
  source = "git::https://gitlab.com/mosar-infra/tf-module-iam.git?ref=tags/v1.0.0"
}
