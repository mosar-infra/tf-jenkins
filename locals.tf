# root/locals.tf

locals {
  vpc_cidr = "13.13.0.0/16"
}

locals {
  cidr_security_groups = {
    internal = {
      name        = "jenkins_internal_sg"
      description = "security group for internal private access"
      ingress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = module.networking.private_subnet_cidrs
        }
      }
    }
  }
}

locals {
  sg_security_groups = {
    private-jenkins = {
      name        = "jenkins_private_jenkins_sg"
      description = "security group for private access to jenkins"
      ingress = {
        http = {
          from            = 8080
          to              = 8080
          protocol        = "tcp"
          security_groups = module.networking.public_security_groups.*.id
        }
        ssh = {
          from            = 22
          to              = 22
          protocol        = "tcp"
          security_groups = module.networking.bastion_security_groups.*.id
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
        ssh = {
          from        = 22
          to          = 22
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}

locals {
  lb_target_groups = {
    mosar = {
      port                = 80
      protocol            = "HTTP"
      path                = "/"
      healthy_threshold   = 2
      unhealthy_threshold = 2
      timeout             = 3
      interval            = 30
    }
    jenkins = {
      port                = 8080
      protocol            = "HTTP"
      path                = "/login"
      healthy_threshold   = 2
      unhealthy_threshold = 2
      timeout             = 3
      interval            = 30
    }
  }
}

locals {
  nodes = {
    # mosar = {
    #   instance_type               = "t2.small"
    #   subnet_ids                  = module.networking.private_subnet.*.id
    #   vpc_security_group_ids      = concat(module.networking.private_mosar_security_groups.*.id, module.networking.internal_security_groups.*.id)
    #   associate_public_ip_address = false
    #   root_block_device_vol_size  = 10
    #   iam_instance_profile        = module.iam.s3_secretsmanager_profile.name
    #   tags_name_prefix            = "mosar_server_"
    #   user_data_file_path         = "./user_data_scripts/install_docker.tpl"
    #   script_vars                 = {}
    #   private_ip                  = local.private_ips["mosar"]
    # }
    # bastion = {
    #   instance_type               = "t2.micro"
    #   subnet_ids                  = module.networking.public_subnet.*.id
    #   vpc_security_group_ids      = module.networking.bastion_security_groups.*.id
    #   associate_public_ip_address = true
    #   root_block_device_vol_size  = 10
    #   iam_instance_profile        = ""
    #   tags_name_prefix            = "bastion_server_"
    #   user_data_file_path         = "./user_data_scripts/empty.tpl"
    #   script_vars                 = {}
    #   private_ip                  = null
    # }
    jenkins = {
      instance_type               = "t3.medium"
      subnet_ids                  = module.networking.private_subnet.*.id
      vpc_security_group_ids      = concat(module.networking.private_jenkins_security_groups.*.id, module.networking.internal_security_groups.*.id)
      associate_public_ip_address = false
      root_block_device_vol_size  = 30
      iam_instance_profile        = module.iam.s3_secretsmanager_profile.name
      tags_name_prefix            = "jenkins_jenkins_server_"
      user_data_file_path         = "./user_data_scripts/setup_jenkins.tpl"
      script_vars                 = { mosar_server_ip = local.private_ips["mosar"] }
      private_ip                  = null
    }
  }
}

locals {
  private_ips = {
    mosar = split("/", cidrsubnet(module.networking.private_subnet_cidr_map[module.networking.private_subnet.*.id[0]], 8, 254))[0]
  }
}

locals {
  lb_tg_attachments = {
    mosar = {
      port = 80
    }
    jenkins = {
      port = 8080
    }
  }
}

locals {
  certificate_domains = {
    mosar-inquisitive = {
      domain = "mosar.inquisitive.nl"
      validation_method = "EMAIL"
    }
    jenkins-inquisitive = {
      domain = "jenkins.inquisitive.nl"
      validation_method = "EMAIL"
    }
  }
}
