output "certificates" {
  value     = module.loadbalancing.certificates
  sensitive = true
}
