#!/bin/bash -xe
echo 'begin install ======================================================================='
until ping -c1 www.google.com &>/dev/null; do
    echo "Waiting for network ..."
    sleep 1
done
yum update -y
yum install -y wget
yum install -y docker
service docker start
systemctl enable --now docker
usermod -a -G docker ec2-user
curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
yum install -y jq
echo GITLAB_USER=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id gitlab-credentials --query SecretString --output text | jq -r .GITLAB_USER) | tee -a /home/ec2-user/env
echo GITLAB_PASSWORD=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id gitlab-credentials --query SecretString --output text | jq -r .GITLAB_PASSWORD) | tee -a /home/ec2-user/env
echo GITLAB_API_TOKEN=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id gitlab-api-token --query SecretString --output text | jq -r .GITLAB_API_TOKEN) | tee -a /home/ec2-user/env
echo DOCKERHUB_USER=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id dockerhub-credentials --query SecretString --output text | jq -r .DOCKERHUB_USER) | tee -a /home/ec2-user/env
echo DOCKERHUB_PASSWORD=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id dockerhub-credentials --query SecretString --output text | jq -r .DOCKERHUB_PASSWORD) | tee -a home/ec2-user/env
echo JENKINS_USER=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id jenkins-credentials --query SecretString --output text | jq -r .JENKINS_USER) | tee -a /home/ec2-user/env
echo JENKINS_PASSWORD=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id jenkins-credentials --query SecretString --output text | jq -r .JENKINS_PASSWORD) | tee -a /home/ec2-user/env
echo JENKINS_URL=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id jenkins-url --query SecretString --output text | jq -r .JENKINS_URL) | tee -a /home/ec2-user/env
echo EC2_SSH=$(aws --region eu-central-1 secretsmanager get-secret-value --secret-id ec2-ssh --query SecretString --output text | jq -r .EC2_SSH) | tee -a /home/ec2-user/env
aws s3 cp s3://devdiv-jenkins/ /home/ec2-user/ --recursive
ssh-keyscan ${mosar_server_ip} >> /home/ec2-user/known_hosts

cd /home/ec2-user && docker-compose up -d
cp /home/ec2-user/known_hosts /home/ec2-user/userdata/jenkins-data/

